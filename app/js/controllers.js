'use strict';

/* Controllers */

//NOTE: we can method chain these as well but I think it is harder to read sometimes when using anonymous functions
//angular.module('myApp.controllers', []).controller...

var app = angular.module('myApp.controllers', []);

/**
 * main page controller
 */
app.controller('MainCtrl', function($scope, DataModel, $location) {
    $scope.datamodel = DataModel;

    //DataModel.validateLogin();

    $scope.showListView = function() {
        $location.path('/listview');
    }
});


/**
 * Login controller
 */
app.controller('LoginCtrl', function($scope, DataModel, $location) {
    window.scope = $scope;

    //for controller as syntax
    this.title = "System Login";

    //login error default to message on the model, this is set by other places in the app
    $scope.errorMsg = DataModel.loginMessage;

    //form model, no need to declare UI is already setting these up.  Just showing so you see where they come from
    $scope.username = '';
    $scope.password = '';

    //login function
    $scope.authenticate = function() {
        //reset the error message
        $scope.errorMsg = "";

        //call out authentication service
        if ($scope.username === "todd" && $scope.password === "tester") {
            //here is where we call a service to give us authentication
            DataModel.UserVO =
                {
                    firstName : "Todd",
                    lastName : "Isaacs",
                    authenticated : true
                };


            console.log("redirect to .." + DataModel.redirectPath);
            //redirect to main view
            $location.path(DataModel.redirectPath);
        } else {
            //set authentication failed state
            $scope.errorMsg = "Invalid Login Credentials"
        }
    }
});

app.controller('ListViewCtrl', function($scope, DataModel, $http) {
    window.scope = $scope;
    $scope.datamodel = DataModel;

    //we can listen to the $routeChangeStart instead of adding to everypage
   // DataModel.validateLogin();

    $scope.getTenDayForecast =  function() {
        if ( DataModel.TenDayForecast) {
            return DataModel.TenDayForecast.forecast.simpleforecast.forecastday;
        } else {
            [];
        }

    };

    //http://api.wunderground.com/api/eaecf3626e6bc477/forecast10day/q/CA/San_Francisco.json

    //NOTE: using jsonp to avoid No 'Access-Control-Allow-Origin' issues also use the JSON_CALLBACK for angular to substitute (angular.callbacks_0)
    var url = 'http://api.wunderground.com/api/eaecf3626e6bc477/forecast10day/q/CA/San_Francisco.json?callback=JSON_CALLBACK';

    //get 10 day forecast
    $http.jsonp(url).
        success(function(data, status, headers, config) {
            // this callback will be called asynchronously
            // when the response is available
            console.log(data);
            DataModel.TenDayForecast = data;
        }).
        error(function(data, status, headers, config) {
            // called asynchronously if an error occurs
            // or server returns response with an error status.
            console.log("Error");
        });
});

