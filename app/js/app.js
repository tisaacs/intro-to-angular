'use strict';


// sets the module and the dependency injection references for the application
//NOTE: angular.module('myApp') is a getter call see
var app = angular.module('myApp', [
  'ngRoute',
  'myApp.controllers'
]);

/**
 * verify we have credentials for this  route change
 */
app.run(function($rootScope, DataModel, $location) {
    $rootScope.$on('$routeChangeStart', function(event, next, current) {

        //determine if we need login
        if (next.$$route && next.$$route.originalPath !== '/login' && !DataModel.isAuthenticated()) {
            //capture where we are going
            DataModel.redirectPath = next.$$route.originalPath;

            //say something
            DataModel.loginMessage = "Authorized Users Only";

            //redirect
            $location.path('/login');

            event.preventDefault();
        }
    });
});


app.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/listview', {templateUrl: 'partials/listview.html'});
  $routeProvider.when('/main', {templateUrl: 'partials/main.html', controller: 'MainCtrl'});
  $routeProvider.when('/login', {templateUrl: 'partials/login.html', controller: 'LoginCtrl', controllerAs: 'ctrl'});
  $routeProvider.otherwise({redirectTo: '/login'});
}]);


/**
 * Application Data Model for shared data across controllers
 */
app.factory('DataModel', function($location){
    var data = {
        //user Object
        UserVO: {},

        /**
         * Verifies the user is authenticated and redirects them to the login if not
         */
        isAuthenticated : function () {
            return this.UserVO.authenticated;
        },

        //This message will be checked by the login controller
        loginMessage : '',

        //default redirect path
        redirectPath : '/main'


        /**
         * Verifies the user is authenticated and redirects them to the login if not
         */
//        ,validateLogin : function () {
//
//            if (!this.UserVO.authenticated) {
//                data.redirectPath = $location.path();
//
//                data.loginMessage = "Authorized Users Only";
//
//                $location.path('/login');
//            }
//        }

    };

    return data;
});
